#!/usr/bin/env fish

set SHORTCUT "Ctrl+Y"
set APP_NAME "Youload"
set DESKTOP_FILE "/usr/share/applications/Youload.desktop"


function is_kwin_present
   if pgrep -x "kwin_x11" >/dev/null || pgrep -x "kwin_wayland" >/dev/null
      return 0; and echo "KDE environment detected."
   else
      return 1; and echo "No KDE environment detected. No shortcut will be set."
   end
end


function shortcut_exists
   set config_file "$HOME/.config/kglobalshortcutsrc"
   if test -f $config_file
       if grep -q "^$APP_NAME=" $config_file
           return 0
       end
   end
   return 1
end


if is_kwin_present
   echo "KDE environment detected. Proceeding with shortcut setup."

   if shortcut_exists
      echo "Warning: The shortcut $SHORTCUT is already assigned to some action."
      echo "The Youload shortcut will not be created to avoid conflicts."
      echo "Please choose a different shortcut and manually set it in System Settings."
   else
      kwriteconfig6 --file kglobalshortcutsrc --group "Custom Shortcuts" --key "$APP_NAME" "kioclient exec $DESKTOP_FILE,none,$APP_NAME"

      kwriteconfig6 --file kglobalshortcutsrc --group "Custom Shortcuts" --key "$APP_NAME Shortcut" "$SHORTCUT,none,$APP_NAME"

      echo "Shortcut $SHORTCUT has been assigned to $APP_NAME."

      if qdbus org.kde.KWin /KWin reconfigure
         echo "KWin configuration reloaded successfully."
      else
         echo "Failed to reload KWin configuration."
         echo "You may need to log out and log back in for the changes to take effect."
      end
   end
else
   echo "KDE environment not detected. Skipping shortcut setup."
end
