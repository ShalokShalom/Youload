function youload
   set url_input (kdialog --title "Youload" --inputbox "Please enter the Video address:")
   if test $status -ne 0
      echo "URL input canceled. Exiting."
      return 1
   end
   
   set quality_input (kdialog --menu "Choose your quality setting" 2160 "2160p" 1440 "1440p" 1080 "1080p" 720 "720p" 480 "480p" OnlyAudio "Only Audio")
   if test $status -ne 0
      echo "Quality selection canceled. Exiting."
      return 1
   end
   
   if string match -rq "OnlyAudio" $quality_input
      yt-dlp $url_input --extract-audio
   else
      set video_quality (string collect -- "--format" "bestvideo[height<=$quality_input]+bestaudio/best[height<=$quality_input]")
      yt-dlp $url_input $video_quality
   end
end
